Categories:Internet
License:GPL-3.0-or-later
Web Site:
Source Code:https://github.com/marcoM32/SeaMapDroid
Issue Tracker:https://github.com/marcoM32/SeaMapDroid/issues

Auto Name:SeaMapDroid
Summary:Browse OpenSeaMap on your phone
Description:
An Android application to consult the libre online nautical maps OpenSeaMap.
.

Repo Type:git
Repo:https://github.com/marcoM32/SeaMapDroid.git

Build:1.0,1
    commit=v1.2.2
    subdir=app
    gradle=yes

Build:1.2.2,23
    commit=v1.2.2
    subdir=app
    gradle=yes

Build:1.3.2,24
    commit=v1.3.2
    subdir=app
    gradle=yes

Build:1.4.2,25
    commit=v1.4.2
    subdir=app
    gradle=yes

Auto Update Mode:Version v%v
Update Check Mode:Tags
Current Version:1.4.2
Current Version Code:25
